package com.example.cryptonotify

import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.example.cryptonotify.services.BackgroundService
import com.example.cryptonotify.services.ForegroundStarter
import com.example.cryptonotify.services.MyServiceConnection
import com.example.cryptonotify.utils.CRYPTO_TOKEN
import com.example.cryptonotify.utils.MY_FIREBASE_REF
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging
import com.squareup.picasso.Downloader


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        FirebaseMessaging.getInstance().isAutoInitEnabled = true
        FirebaseInstanceId.getInstance().instanceId
                .addOnCompleteListener(OnCompleteListener { task ->
                    if (!task.isSuccessful) {
                        Log.w(ContentValues.TAG, "getInstanceId failed", task.exception)
                        return@OnCompleteListener
                    }
                    // Get new Instance ID token
                    val token = task.result?.token
                    MY_FIREBASE_REF.child(CRYPTO_TOKEN).setValue(token, null)
                })

        FirebaseMessaging.getInstance().subscribeToTopic("crypto")

//        val i = Intent(this, ForegroundStarter::class.java)
//        this.startService(i)


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val bindIntent = Intent(this, BackgroundService::class.java)
            val serviceConnection = MyServiceConnection()
            val intent = Intent(this, BackgroundService::class.java)
//            bindService(bindIntent, serviceConnection, Context.BIND_AUTO_CREATE)
            startForegroundService(intent)
        } else {
            startService(intent)
        }

    }
}