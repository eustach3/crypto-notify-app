package com.example.cryptonotify.services

import android.annotation.SuppressLint
import android.app.IntentService
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.media.MediaPlayer
import android.os.Binder
import android.os.Build
import android.os.VibrationEffect
import android.os.Vibrator
import com.example.cryptonotify.R
import com.example.cryptonotify.utils.fireNotification


class NotificationService : IntentService("NotificationService") {

    @SuppressLint("NewApi")
    private fun createChannel() {


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            // Create the NotificationChannel, but only on API 26+ because
            // the NotificationChannel class is new and not in the support library

            val context = this.applicationContext
            val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

            val importance = NotificationManager.IMPORTANCE_HIGH
            val notificationChannel = NotificationChannel(CHANNEL_ID, CHANNEL_NAME, importance)
            notificationChannel.enableVibration(true)
            notificationChannel.setShowBadge(true)
            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.parseColor("#e8334a")
            notificationChannel.description = ""
            notificationChannel.lockscreenVisibility = Notification.VISIBILITY_PUBLIC
            notificationManager.createNotificationChannel(notificationChannel)
        }

    }

    companion object {

        const val CHANNEL_ID = "com.lineage2gods.l2gods.98"
        const val CHANNEL_NAME = "Lineage 2 Gods - Notification"
    }


    override fun onHandleIntent(intent: Intent?) {

        //Create Channel
        createChannel()


        var timestamp: Long = 0
        var pairName = ""
        var positionStrat = ""
        var breakoutStrat = ""
        if (intent != null && intent.extras != null) {
            pairName = intent.extras!!.getString(com.example.cryptonotify.utils.pairName)!!
            positionStrat = intent.extras!!.getString(com.example.cryptonotify.utils.positionStrat)!!
            breakoutStrat = intent.extras!!.getString(com.example.cryptonotify.utils.breakoutStrat)!!
        }
        val isEmergency = positionStrat.equals("emergency",true)
        if (pairName.isNotEmpty()) {
            val v = getSystemService(VIBRATOR_SERVICE) as Vibrator
// Vibrate for 500 milliseconds
// Vibrate for 500 milliseconds
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                v.vibrate(VibrationEffect.createOneShot(5000, VibrationEffect.DEFAULT_AMPLITUDE))
            } else {
                //deprecated in API 26
                v.vibrate(5000)
            }
            val alarm = if (isEmergency) R.raw.alarm_emergency else R.raw.alarm_sound
            var mediaPlayer = MediaPlayer.create(this, alarm)
            mediaPlayer.setOnCompletionListener({ mp ->
                mp.reset()
                mp.release()
//                mediaPlayer.reset()
//                mp.release()
                mediaPlayer = null
            })
            mediaPlayer.start()
            fireNotification(this, false, pairName, timestamp, positionStrat, breakoutStrat)
        }
    }
}