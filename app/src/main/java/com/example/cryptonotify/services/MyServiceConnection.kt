package com.example.cryptonotify.services

import android.content.ComponentName
import android.content.ServiceConnection
import android.os.IBinder

class MyServiceConnection : ServiceConnection {
    var binder: BackgroundService.SimpleBinder? = null

    override fun onServiceConnected(name: ComponentName?,
                                    binder: IBinder?) {
        this.binder = binder as BackgroundService.SimpleBinder
    }

    override fun onServiceDisconnected(name: ComponentName?) {
        binder = null
    }
}