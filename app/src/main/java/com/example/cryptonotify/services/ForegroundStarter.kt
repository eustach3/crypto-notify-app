package com.example.cryptonotify.services

import android.R
import android.app.IntentService
import android.app.Notification
import android.content.Intent
import androidx.core.app.NotificationCompat


class ForegroundStarter : IntentService("foreground") {
    private val FOREGROUND_ID: Int = 1553

    override fun onHandleIntent(intent: Intent?) {
        startForeground(
            FOREGROUND_ID,
            buildForegroundNotification("bot")
        )
    }

    private fun buildForegroundNotification(filename: String): Notification? {
        val b = NotificationCompat.Builder(this)
        b.setOngoing(true)
            .setContentTitle("Crypto")
            .setContentText(filename)
            .setSmallIcon(R.drawable.stat_sys_download)
            .setTicker("Listening")
        return b.build()
    }
}