package com.example.cryptonotify.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.example.cryptonotify.services.NotificationService
import com.example.cryptonotify.utils.breakoutStrat
import com.example.cryptonotify.utils.raidNotifName
import com.example.cryptonotify.utils.positionStrat
import com.example.cryptonotify.utils.pairName

class AlarmReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        val service = Intent(context, NotificationService::class.java)
        service.putExtra(pairName, intent.getStringExtra(pairName))
        service.putExtra(positionStrat, intent.getStringExtra(positionStrat))
        service.putExtra(breakoutStrat, intent.getStringExtra(breakoutStrat))
        if (intent.extras?.getLong("timestamp")!! >0) {
            service.putExtra("timestamp", intent.getLongExtra("timestamp", 0))
            context.startService(service)
        }
    }
}