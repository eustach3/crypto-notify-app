package com.example.cryptonotify.utils

import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.media.RingtoneManager
import android.os.Build
import android.view.View
import android.view.ViewGroup
import android.widget.ListAdapter
import android.widget.ListView
import com.example.cryptonotify.MainActivity
import com.example.cryptonotify.R
import com.google.firebase.database.FirebaseDatabase
import com.example.cryptonotify.services.NotificationService
import java.util.*

val preferencesUser: String = "userName"
val preferenceRaidNotif: String = "raidNotification"
val preferenceRaidSpawn: String = "raidSpawn"

val MY_FIREBASE_REF = FirebaseDatabase.getInstance().reference
const val CRYPTO_TOKEN = "cryptoToken"
val TOKENS = "TOKENS"
const val CHARS = "chars"
const val RAIDS = "raids"
const val FRIENDS = "friends"
const val CLANS = "clan"
const val raidNotifName = "raidName"
const val pairName = "userNotifName"
const val shouldNotifyFriends = "notifyFriendsOnline"
const val shouldNotifyClans = "notifyClansOnline"
const val lastSelectedCharPos = "lastSelectedChar"
const val lowPerformance = "lowPerformance"
const val raidSpinner = "raidNotificationSpinner"
const val positionStrat = "raidSpawnTimeText"
const val breakoutStrat = "breakoutStrat"


fun fireNotification(
    context: Context,
    isRaid: Boolean,
    pairName: String,
    timestamp: Long,
    positionStrat: String,
    breakoutStrat: String
) {
    lateinit var mNotification: Notification
    val mNotificationId: Int = 1000

    val notifyIntent = Intent(context, MainActivity::class.java)
    val isEmergency = positionStrat.equals("emergency",true)

    val title = if (isEmergency) "!!!Emergency, cannot close order: $pairName" else "Pair: $pairName"
    val message = if (isEmergency) "!!!Emergency, cannot close order: $pairName" else "$pairName, $positionStrat, $breakoutStrat"

    notifyIntent.putExtra("title", title)
    notifyIntent.putExtra("message", message)
    notifyIntent.putExtra("notification", true)

    notifyIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK

    val calendar = Calendar.getInstance()
    calendar.timeInMillis = timestamp


    val pendingIntent =
        PendingIntent.getActivity(context, 0, notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT)
    val res = context.resources
    val uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {


        mNotification = Notification.Builder(context, NotificationService.CHANNEL_ID)
            // Set the intent that will fire when the user taps the notification
            .setContentIntent(pendingIntent)
            .setSmallIcon(R.drawable.ic_launcher_background)
            .setLargeIcon(BitmapFactory.decodeResource(res, R.drawable.ic_launcher_background))
            .setAutoCancel(true)
            .setContentTitle(title)
            .setStyle(
                Notification.BigTextStyle()
                    .bigText(message)
            )
            .setContentText(message).build()
    } else {

        mNotification = Notification.Builder(context)
            // Set the intent that will fire when the user taps the notification
            .setContentIntent(pendingIntent)
            .setSmallIcon(R.drawable.ic_launcher_background)
            .setLargeIcon(BitmapFactory.decodeResource(res, R.drawable.ic_launcher_background))
            .setAutoCancel(true)
            .setPriority(Notification.PRIORITY_MAX)
            .setContentTitle(title)
            .setStyle(
                Notification.BigTextStyle()
                    .bigText(message)
            )
            .setSound(uri)
            .setContentText(message).build()
    }

    val notificationManager
            = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    // mNotificationId is a unique int for each notification that you must define
    notificationManager.notify(mNotificationId, mNotification)
}

fun setListViewHeightBasedOnChildren(listView: ListView) {
    val listAdapter: ListAdapter = listView.getAdapter() ?: return
    var totalHeight = 0
    var i = 0
    val len: Int = listAdapter.getCount()
    while (i < len) {
        val listItem: View = listAdapter.getView(i, null, listView)
        listItem.measure(0, 0)
        totalHeight += listItem.measuredHeight+20
        i++
    }
    val params: ViewGroup.LayoutParams = listView.getLayoutParams()
    params.height = (totalHeight
            + listView.getDividerHeight() * (listAdapter.getCount()))
    listView.setLayoutParams(params)
}
