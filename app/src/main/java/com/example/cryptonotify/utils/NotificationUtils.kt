package com.example.cryptonotify.utils

import android.app.Activity
import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.annotation.Nullable
import com.example.cryptonotify.receivers.AlarmReceiver
import java.util.*
import java.util.concurrent.TimeUnit

class NotificationUtils {
    /**
     * Schedule notification through alarm service. Put raidId >99 if you want to schedule player instance notification
     */
    fun setNotification(
        timeInMilliSeconds: Long,
        activity: Context,
        raidId: Int,
        allowWhileIdle: Boolean,
        @Nullable
            pairName: String,
        @Nullable
            positionStrat: String,
        breakoutStrat: String
    ) {
        val isFriend : Boolean = raidId>99
        if (timeInMilliSeconds > 0) {

            val alarmManager = activity.getSystemService(Activity.ALARM_SERVICE) as AlarmManager
            val alarmIntent = Intent(activity.applicationContext, AlarmReceiver::class.java) // AlarmReceiver1 = broadcast receiver
            alarmIntent.putExtra(com.example.cryptonotify.utils.pairName, pairName)
            alarmIntent.putExtra("timestamp", timeInMilliSeconds)
            alarmIntent.putExtra(com.example.cryptonotify.utils.positionStrat, positionStrat)
            alarmIntent.putExtra(com.example.cryptonotify.utils.breakoutStrat, breakoutStrat)
//            if (!allowWhileIdle) alarmIntent.putExtra(raidNotifName, instanceName )
            val calendar = Calendar.getInstance()
            calendar.timeInMillis = timeInMilliSeconds

            val pendingIntent = PendingIntent.getBroadcast(activity, raidId, alarmIntent, PendingIntent.FLAG_CANCEL_CURRENT)
            if (allowWhileIdle) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    alarmManager.setAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, calendar.timeInMillis, pendingIntent)
                }
                else {
                    alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.timeInMillis, pendingIntent)
                }
            }
            else {
                val min30 = TimeUnit.MINUTES.toMillis(30)
                alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + min30,
                    min30, pendingIntent)
            }
//            Toast.makeText(activity, "Notification has been set!", Toast.LENGTH_LONG).show()
        }
    }

    fun cancelNotification(context: Context, raidId: Int) {
        val alarmManager = context.getSystemService(Activity.ALARM_SERVICE) as AlarmManager
        val alarmIntent = Intent(context.applicationContext, AlarmReceiver::class.java) // AlarmReceiver1 = broadcast receiver

        val pendingIntent = PendingIntent.getBroadcast(context, raidId, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT)
        alarmManager.cancel(pendingIntent)
    }
}